#!/usr/bin/env python
import urllib2
import json
products_dictionary = {}
ondemand_dictionary = {}

response = urllib2.urlopen('https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/index.json')
f = json.load(response)
#print data
for k,v in f['products'].items():
    products_dictionary[k] = v
for k2,v2 in f['terms']['OnDemand'].items():
    ondemand_dictionary[k2] = v2



product_meet_requirement_dict = {}
for k3,v3 in products_dictionary.items():
    if v3["productFamily"]=="Compute Instance" and v3["attributes"]["location"]=="Asia Pacific (Sydney)" and v3["attributes"]["tenancy"]=="Shared" and v3["attributes"]["servicecode"]=="AmazonEC2":
        product_meet_requirement_dict[k3] = v3


with open("testfile.txt","w") as f_f:
#json.dump((required_price,instanceType,operatingSystem),f_f)
    price_dimension_dict={}
    for k4,v4 in product_meet_requirement_dict.items():
        required_price= ondemand_dictionary[k4][k4+'.JRTCKXETXF']['priceDimensions'][k4+'.JRTCKXETXF'+'.6YS6EN2CT7']['pricePerUnit']["USD"]
        instanceType=product_meet_requirement_dict[k4]["attributes"]["instanceType"]
        operatingSystem=product_meet_requirement_dict[k4]["attributes"]["operatingSystem"]
        for each in required_price:
         f_f.write(each)
        f_f.write(",")
        for each in instanceType:
         f_f.write(each)
        f_f.write(" ")
        for each in operatingSystem:
         f_f.write(each)
        f_f.write("\n")
f_f.close()
